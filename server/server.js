const express=require("express")
const cors = require("cors");

const app=express();

app.use(cors());


const userRouter=require("./api/user.router")
app.use(express.json())
app.use('/api/users',userRouter)

const port=process.env.PORT

app.listen(port,()=>{
    console.log(`listening on port ${port} `)
})