const router=require("express").Router();


const {createUser,getUsers,getUserById, UpdateUser, deleteUser,login,signup,createInfo,
       getAllUsersInfo,getUserInfoById,UpdateUserInfo,deleteUserInfo,getInfoByUserId}=require('./user.controller');


const {checkToken}=require("../authentication/token.validation");




router.post('/',checkToken,createUser);

router.post("/postinfo",checkToken,createInfo)


router.get('/',getUsers)

router.get("/userinfo",getAllUsersInfo)



router.get("/userinfo/:id",checkToken,getUserInfoById)

router.get('/:id',checkToken,getUserById)



router.put('/',checkToken,UpdateUser)

router.put('/userinfo',checkToken,UpdateUserInfo)



router.delete('/',checkToken,deleteUser)

router.delete('/userinfo',checkToken,deleteUserInfo)



router.post('/login',login)

router.post("/signup",signup)

router.get('/infoByUID/:id',getInfoByUserId)





module.exports=router
