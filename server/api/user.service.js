
const pool=require("../config/db")

module.exports={

    create:(data,callback)=>{
        console.log(data)
        pool.query(`INSERT into User (name,email,password) values(?,?,?)`,
        [
            data.name,data.email,data.password
        ],
        (err,results)=>{
           if(err){
                return callback(err)
           } 

             return  callback(null,results)
           
        }
        )
    },


    getUsers:callback=>{
        pool.query(`select * from User`,(err,results)=>{
            if(err){
                return callback(err)
            }
            return callback(null,results)
        })
        },


        getUserById:(id,callback)=>{
            pool.query(`select * from User where id=?`,[id],
            (err,results)=>{
                if(err){
                     return callback(err)
                }
                return callback(null,results)
            }
            )
        },
    
  
        UpdateUser:(data,callback)=>{
            console.log(data)
            pool.query(`update User set name=?,email=?,password=? where id=?`,
            [
                data.name,
                data.email,
                data.password,
                data.id
            ],
            (err,results)=>{
                if(err){
                    return callback(err)
                }
                return callback(null,results)
            }
            )
        },


        deleteUser:(data,callback)=>{
            console.log(data.id)
            pool.query(`delete from User where id=?`,[data.id],
            (err,results)=>{
                if(err){
                    return callback(err)
                }
                return callback(null,results[0])
            })
        },
        

        getUserByEmail:(email,callback)=>{
            console.log(email)
            pool.query(`select * from User where email=? `,[email],
            (err,results)=>{
                if(err){
                  return  callback(err)
                }
                return callback(null,results[0])
            })
        },


        signup:(data,callback)=>{
            console.log(data)
            pool.query(`INSERT into User (name,email,password) values(?,?,?)`,
            [
                data.name,data.email,data.password
            ],
            (err,results)=>{
               if(err){
                    return callback(err)
               } 
    
                 return  callback(null,results)
               
            }
            )
        },


        createUserInfo:(data,callback)=>{
            console.log()
         pool.query(`INSERT INTO UserInfo (title,description,Userid) VALUES(?,?,?)`,
               [data.title,data.description,data.Userid],
          (err,results)=>{
               if(err){
                    return callback(err)
               } 
    
                 return  callback(null,results)
               
            }
            
            )
        },
 
        getAllUInfo: callback=>{
            pool.query("select * from UserInfo",(err,results)=>{
                if(err){
                    return callback(err)
                }
                return callback(null,results)
            })
            },
    

            getUserInfoById:(id,callback)=>{
                pool.query(`select * from UserInfo where id=?`,[id],
                (err,results)=>{
                    if(err){
                         return callback(err)
                    }
                    return callback(null,results)
                }
                )
            },



            UpdateUserInfo:(data,callback)=>{
                console.log(data)
                pool.query(`update UserInfo set title=?,description=? where id=?`,
                [
                    data.title,
                    
                    data.description,
                    data.id
                ],
                (err,results)=>{
                    if(err){
                        return callback(err)
                    }
                    return callback(null,results)
                }
                )
            },


            deleteUInfo:(data,callback)=>{
                console.log(data.id)
                pool.query(`delete from UserInfo where id=?`,[data.id],
                (err,results)=>{
                    if(err){
                        return callback(err)
                    }
                    return callback(null,results[0])
                })
            },


        getInfoByUserId:(id,callback)=>{
         const UID=id
         pool.query(`select * from UserInfo where Userid=?`,[UID],
         (err,results)=>{
            if(err){
                 return callback(err)
            }
            
            return callback(null,results)
            
        })
        

        }
}